from alpine:3.8
run apk add --no-cache --update mongodb=3.6.7-r0 
entrypoint ["mongod", "--noauth", "--dbpath", "/mongo/data/db", "--host","mongo:27017"] 
expose 27017:27017
